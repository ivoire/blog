#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'R\xe9mi Duraffort'
SITENAME = u'Spill the beans'
SITEURL = 'https://blog.duraffort.fr'

PATH = 'content'

ARTICLE_URL = 'post/{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'post/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'


TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'

THEME = "tuxlite_tbs"

# RSS/ATOM
FEED_ALL_ATOM = 'feed/atom'
FEED_ALL_RSS = 'feed/rss'
CATEGORY_FEED_ATOM = 'feed/category/%s/atom'
CATEGORY_FEED_RSS = 'feed/category/%s/rss'
TAG_FEED_ATOM = 'feed/tag/%s/atom'
TAG_FEED_RSS = 'feed/tag/%s/rss'
AUTHOR_FEED_ATOM = 'feed/author/%s/atom'
AUTHOR_FEED_RSS = 'feed/author/%s/rss'
TRANSLATION_FEED_ATOM = None

# Blogroll
LINKS = (('FramaGit', 'https://framagit.org/u/ivoire/'),
         ('GitHub', 'https://github.com/ivoire/'),
         ('OpenHub', 'https://www.openhub.net/accounts/ivoire'))

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

STATIC_PATHS = ['rsc', 'index.php', 'favicon.ico']
