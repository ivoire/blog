Title: Free music and VLC
Date: 2010-08-18 15:23
Category: VLC media player
Tags: Extension, lua, VideoLAN, VLC
Slug: Free-music-and-VLC

Actually more and more artists are following the Open Source movement
and create really nice content (both audio and video). Since the latest
release of VLC media player (1.1.0), thanks to VLC extensions, we added
the ability to access selections of Free Music directly from the
playlist. Let's have a look at these contents.

Free music inside VLC
=====================

In the last release of VLC media player, we introduced the ability to browse
some selections of free music. The following screenshot shows VLC playing a
selection of the 20 most popular albums on
[jamendo](http://www.jamendo.com)

![Free music inside VLC: The postem]({filename}/rsc/vlc_free_music_the_postem.png)

Actually in the VLC playlist you can access to:

* A selection of music from [jamendo](http://www.jamendo.com)
* A selection of famous free songs from [Free music
  chart](http://www.archive.org/details/free-music-charts)

These selections can be found in the playlist selector under the *Internet*
item, under:

* *Free music charts*: 100 most famous free song (25 per month)
* *Jamendo Selection*:
    * Top 100 most popular songs of the week on [jamendo](http://www.jamendo.com)
    * Top 20 most popular albums of the week on [jamendo](http://www.jamendo.com)

This way you can listen to some selections of free music and discover new
artists and groups that you might have not ear about before.

What's *Free music*
===================

A song can be classified as *Free music* if the artist choose to distribute the
song under a license that let some rights to the listener. For example, your
are never allowed to redistribute Non-Free Music whereas a big part of the
*Free Music* can be redistributed.


The big different between *Free Music* and the others is the ability for the
artist to give more right to the listener. The artists can choose the rights to
give to the listener when choosing the license for their work.

![Creative Commons]({filename}/rsc/cc-logo.jpg)

The *Creative Commons* is a good choice for *Free Music* as this given the
choice for the rights to give to the users of the music:

* Right (or not) to redistribute their work for commercial usage
* Right (or not) to modify their work

I will soon write an article about the *Creative Commons*, but you can first
have a look [here](http://creativecommons.org).

Jamendo
=======

Jamendo is one of the first (maybe the first) website to promote and help
artist to distribute their work. Every content you can get on Jamendo is free
and can be downloaded legally. This is a great opportunity to discover new
artists and groups.

Moreover as VLC can now give you a selection of some songs and albums, you
don't have any more excuse not to try it...
