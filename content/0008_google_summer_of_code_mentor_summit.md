Title: Google Summer of Code: Mentor Summit
Date: 2010-10-21 10:34
Category: VideoLAN
Tags: Free Software, GSoC, VideoLAN, VLC
Slug: Google-Summer-of-Code:-Mentor-Summit

Google Mentor Summit
====================

A quick note to announce that we will be at the *Google Mentor Summit* in
California this week-end. This meeting is organized by Google to close the
*Google Summer of Code* and to meet developers from different Free Softwares
projects.

Two developers from the VideoLAN project and VLC media player will be present
to speak about multimedia, Free Software and many more things.

I will take some picture of the event and post them back here afterward...
