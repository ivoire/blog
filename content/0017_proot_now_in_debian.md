Title: PRoot now in Debian
Date: 2013-08-20 13:40
Category: PRoot
Tags: Debian, PRoot
Slug: PRoot-now-in-Debian

A really short message to inform you that [PRoot](http://proot.me) is now part
of Debian Sid.

You can now install PRoot with `apt-get install proot`.

Enjoy !
