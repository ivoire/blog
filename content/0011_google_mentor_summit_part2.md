Title: Google Mentor Summit (part2)
Date: 2010-11-11 19:23
Category: VideoLAN
Tags: Free Software, GCi, GSoC, VideoLAN, VLC
Slug: Google-Mentor-Summit-(part2)

We went to the *Google Mentor Summit* in California for the week-end, let's
talk a bit about it ...

The Google Mentor Summit
========================

Every year, Google organizes the *Google Summer of Code*. This event is an
opportunity for students to work on Open Source projects during the summer and
being payed by Google.

At the end of the program, Google invites two person of each organization that
has taken part in the Google Summer of Code for a week-end at Google's Campus
in California. This event is called the *Google Mentor Summit.*

Friday 22th
===========

We took the flight from Paris to San Francisco airport. In the plane we met
some persons working for other open source projects: that helped spending the
11 hours until the end of the flight.

A bus was settled to take us from the airport to the hotel. Then we went to the
lunch organized by Google, helped us meeting people from other projects. Then
Google had organized a lunch

Saturday 23th
=============

![Google]({filename}/rsc/IMG_2213_small.JPG))

At 9 am, every mentor went from the hotel to the Google Campus for the
welcoming conference. The goal of this conference was to explain the main
concept of the Google Mentor Summit: [the
Unconference](http://en.wikipedia.org/wiki/Unconference).

The principle is really simple: every person who wants to talk about a subject
writes down the subject on a paper, then everyone votes for his preferred
topics. According to the their popularity, a room is attributed to each
subject.

![Google]({filename}/rsc/IMG_2192_small.JPG)

I decided to attend the following topics:

* Static code analysis
* Open Source Multimedia (suggested by [jb](http://www.jbkempf.com/blog/))
* Chocolate Session
* Google Code In

Static code analysis
--------------------

The discussion was mainly about the different [Static Code
Analyzers](http://en.wikipedia.org/wiki/Static_code_analysis) that can be used
to improve the quality of Open Source softwares. We examined the available
tools:

* [Splint](http://www.splint.org/): basic static analyzer that can
  find classical mistakes
* [Cppcheck](http://sourceforge.net/apps/mediawiki/cppcheck/index.php?title=Main_Page):
  I used this one for a long time and it reported valuable errors on VLC source
  code
* [Clang](http://clang.llvm.org/): a more advanced static analyzer
  based on LLVM that we currently use for VLC
* [Coverity](http://www.coverity.com/products/coverity-prevent.html):
  a commercial static analyzer. The company used to scan VLC source code but
  stopped some years ago. The results are often interesting.
* [Coccinelle](http://coccinelle.lip6.fr/): this tool is mainly
  used by the Linux kernel and I found some bugs in VLC using it

Open Source Multimedia
----------------------

This meeting, suggested by the VideoLAN chairman (jean-baptiste Kempf) gathered
people mainly from [BeagleBoard](http://beagleboard.org/),
[FFmpeg](http://www.ffmpeg.org/), [Mixx](http://www.mixxx.org/),
[RockBox](http://www.rockbox.org/) and [VLC](http://www.videolan.org/).

The meeting was recorded and is available on
[bambuser](http://bambuser.com/channel/Martin/broadcast/1112807).

We talked about:

* Hardware acceleration for Embedded Systems: how do we access it from
  an application point of view? It seems that Android does not allow
  the use of the hardware decoder nor does iOS. That's really a shame
  not being able to use the hardware to decode video.
* Relations between Rockbox and FFmpeg: some code from FFmpeg has been
  optimized for the kind of embedded systems Rockbox runs. Merging
  back the code in FFmpeg seems a good practice but the goal of both
  projects makes it difficult.

The discussions continued for a long time after the available time slot outside
of the room :)

Chocolate Session
-----------------

This is now a tradition (only two years old as far as I understood, but already
a tradition :)), Robert Kaye, founder of [MusicBrainz](http://musicbrainz.org/)
brought 9kg of chocolate from different countries and brought everyone together
to share chocolate.
This was great!

Google Code In
--------------

During this conference, Carol Smith announced the new Google project called
*Google Code in*. This new contest is designed to help 13 to 17 year old
children getting involved in Open Source projects. The tasks are not only
focused on coding but also on improving the documentation, writing examples,
tutorials, ... A lot of stuffs that Open Source projects miss.

In a previous article ([VideoLAN accepted for
GCI]({filename}0010_videolan_accepted_for_google_code_in.md)) I announced
VideoLAN had been selected for the Google Code In. More information are
available in the previous article.

Sunday 24th
===========

The second day, I went to fewer talks but had more time talking with other open
source developers.

I attended to the following sessions:

* Advanced Trolling Techniques
* Tour of the Google Campus

Advanced Trolling Techniques
----------------------------

This session was absolutely not serious and really funny: it explained some
important things about trolling! (If you don't know what a *troll* is, have a
look at the Wikipedia article about [Trolls on
internet](http://en.wikipedia.org/wiki/Troll_%28Internet%29)).

The goal was to teach us how to protect our communities against trolls and
obviously how to troll other communities :). The speakers came up with nice
graphics to support their theory like this one about the complexity of trolls:

![Complexity]({filename}/rsc/IMG_2202_small.JPG)

Tour of the Google campus
-------------------------

We had a commented trip through the Google campus and some time to take
pictures. We encountered a big Android robot, a T. rex and sculptures:

![Google]({filename}/rsc/IMG_2217_small.JPG)

![Google]({filename}/rsc/IMG_2218_small.JPG)

![Google]({filename}/rsc/IMG_2220_small.JPG)

![Google]({filename}/rsc/IMG_2221_small.JPG)

Group shot
----------

After the traditional ending session, we took a group shot (you can click on
the picture if you want to find us):

[![Mentors]({filename}/rsc/gms2010_small.jpg)]({filename}/rsc/gms2010.jpg)

The picture comes from the [Google Open Source
blog](http://google-opensource.blogspot.com/2010/11/2010-google-summer-of-code-mentor.html)).

Monday 25th
-----------

Last day in San Francisco after only 3 days. I have not seen any part of SF
except the Google Campus and the Hotel. That's a shame but I will come back
another time to visit SF.

Just before the flight, we added a meeting with a company that provides video
content. We discussed of a possible collaboration between VLC and them. This
led to the new services discovery script I commited some days ago
([Channels.com](http://git.videolan.org/?p=vlc.git;a=commit;h=ab20299e91701579af48e15316f8c016317e987f)).
You will be able to try this new service in the incoming version: VLC 1.1.5
