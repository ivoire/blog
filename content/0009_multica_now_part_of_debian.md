Title: Multicat now part of Debian
Date: 2010-10-31 12:16
Category: VideoLAN
Tags: Debian, Multicat, VideoLAN
Slug: Multicat-now-part-of-Debian

In another post, I told you about a nice tool called
[Multicat]({filename}0005_multicat.md).

Due to the the Freeze of Debian, Multicat was waiting in the NEW queue.  This
state changed yesterday has the *ftp-masters* uploaded the package to the
unstable version of Debian.

If you run a Debian unstable you can now install Multicat like any other tools.
For every other people, just grab the .deb
[package](http://packages.debian.org/sid/multicat) that correspond to you
architecture and install it. You must be able to install it on older version of
Debian as well.
