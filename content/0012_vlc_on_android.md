Title: VLC on Android
Date: 2011-02-02 19:35
Category: VLC media player
Tags: Android, Compilation, VideoLAN, VLC
Slug: VLC-on-Android

A lot of people are asking about the status of VLC media player on Android. We
usually answered that we are working on it. Now that some good progresses has
been done, lets look at the current status of VLC media player for Android.

Current status
==============

After two months of work, VLC media player is working quite well on Android
based systems. The project still requires a lot of work before any release but
we now know that running VLC media player on Android is achievable.

![VLC on Android]({filename}/rsc/vlc_android_first.png)

At the moment VLC media player was mainly tested in the Android emulator, on a
Nexus One, a Nexus S and an HTC Desire. As soon as we have a beta version we
will ask every people who want to test the application to test it and to report
any issue they encounter. This way we will be able to validate VLC media player
on different phones and tablets.

Current work
============

We are currently mainly working on two parts: the video output and the audio
output. These two modules are needed by VLC to render decoded images on the
screen and to output sound.

Video output
------------

The video output developed by Adrien Maglo (alias Magsoft) is now able to
output video directly into an OpenGL surface. This is the second video output
module created by Adrien for VLC on Android. This second version is able to run
on any version of Android and not only Gingerbread (2.3) but is a bit less
efficient. This trade of is very important because few phones and tablets are
running Android Gingerbread at the moment.

Just two screenshots that show VLC decoding and drawing a trailer of Mr and Mrs
Smith (h264 avc1 and MPEG AAC) on my Nexus One:

![VLC playing a video]({filename}/rsc/vlc_android_mr_smith_small.jpg)

![VLC playing a video]({filename}/rsc/vlc_android_mrs_smith_small.jpg)

Audio output
------------

A first audio output module was developed by Dominique Martinet (alias
Asmadeus). This module was based on OpenSL ES which is quit convenient and
efficient but OPENSL ES is only available on Android 2.3. This is the reason
why we decided to write a new audio output module that might work on any
Android devices regardless of the version.

For this reason, I (Rémi Duraffort, alias ivoire) am developing a second audio
output module for every Android versions. The work is barely started so we
cannot show you any video at the moment.

To be done
==========

Improving the Video output
--------------------------

The video output must be improved to handle some specials cases like the
rotation of the device or the power saving mode (and many more).

Some work must also be done to improve handling of the aspect ratio and
rendering performance.

Audio output
------------

As said before, the next goal is to finish the audio output module to handle
audio correctly. This module requires some work to be able to output audio for
any Android version.

Graphical User Interface
------------------------

As you saw in the screenshots, the GUI was only made to help us testing both
the audio and video output modules. Before any beta version, we must create a
nice GUI that allows users to select the right media or stream to play. We will
work on this task as soon as the audio and video output work well.

Porting libraries
-----------------

VLC media player is relying on a set of libraries for a lot of functions like
decoding, encoding, getting meta-datas (id3 tags), ...

To enable these functionalities we must port the underlying libraries to
Android based devices. This work is not began yet but as for now we already
have a lot of libraries (like [FFmpeg](http://www.ffmpeg.org/)) that allows us
to decode many formats like: mpeg, h264, mp4 or ogg. So this task does not have
priority.

Optimizations
-------------

The last task is to improve VLC media player performance by optimizing it for
the devices it will run on. Every Android devices uses ARM processors which
imply some specific optimizations that are currently not activated. Some
devices also provide ARM NEON instructions that VLC can use to speedup
decoding.

Summary
=======

As a conclusion, you can notice that VLC media player on Android is working
quite well. Anyway we are not ready to release a version until some months
because we want something stable and nice to use. Be patient, we are working on
it, even if this job is only done on our free time.

I will write another article when some interesting news about VLC and Android
come out.
