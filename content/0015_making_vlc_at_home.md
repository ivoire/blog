Title: Making VLC at home
Date: 2012-05-04 23:23
Category: PRoot
Tags: Compilation, PRoot, VLC, Linaro
Slug: Making-VLC-at-home

In my [previous article]({filename}0014_proot_sorcery.md), I showed a way to
setup and jump into a root file system in order to test a software in this
environment.

Using [PRoot](http://proot.me), such task looks really easy and does not
require any privileges. We will now finish this setup in order to build and
test VLC media player in this new root file system.

Setting up the RootFS
=====================

In the [previous article]({filename}0014_proot_sorcery.md) we had downloaded
and configured a Debian root file system ready to be used with
[PRoot](http://proot.me).

In order to build and test VLC media player in this environment, we must
install VLC dependencies. For instance in a Debian system, I already explain
the procedure in an article about the [compilation for Linux
users]({filename}/0007_compiling_vlc_for_linux_people.md).

The first step is to install theses dependencies using **apt-get**:

    :::shell
    $ proot -b /etc/resolv.conf -b /dev -b /proc
            -b /sys -0 debian-6.0-x86_64 /bin/bash
    ~ echo "deb-src http://ftp.debian.org/debian squeeze main contrib non-free" >> /etc/apt/sources.list
    ~ apt-get update
    Hit http://security.debian.org squeeze/updates Release.gpg
    Hit http://ftp.debian.org squeeze Release.gpg
    Ign http://security.debian.org/ squeeze/updates/contrib Translation-en
    [...]
    Fetched 4655 kB in 2s (1955 kB/s)
    Reading package lists... Done
    ~ apt-get build-dep vlc
    Reading package lists... Done
    Building dependency tree
    Reading state information... Done
    The following NEW packages will be installed:
    [...]
    [a long list of packages]
    [...]
    0 upgraded, 410 newly installed, 0 to remove and 0 not upgraded.
    Need to get 195 MB of archives.
    After this operation, 611 MB of additional disk space will be used.
    Do you want to continue [Y/n]?

Pay attention to some packages, like dbus or x11-common, that could not be
installed without the right bindings. In fact dbus install script require
access to **/var/run/dbus/**, while x11-common require to not bind **/tmp**
with a working X11 process as it contains files that belong to root and that
should be modified during installation.

In order to complete the previous command, you should:

* bind /var/run/dbus
* NOT bind /tmp

**apt-get** will install a list of packages that were required to compile the
version of VLC media player normally installed in the host distribution (vlc
version 1.1.3). This mean that some library will be missing. The following
packages are also needed:

* git
* autoconf
* libspeexdsp-dev
* libxcb-composite0-dev

Building VLC
============

You need the code source of VLC media player in this host root file system. I'm
personally a bit lazy and I already have the sources somewhere else on my
computer so I just bind it in the host root file system:

    :::shell
    $ proot -b /etc/resolv.conf -b /dev -b /proc
            -b /sys -b /path/to/vlc/git:/media/vlc.git
            debian-6.0-x86_64 /bin/bash
    ~ cd somewhere
    ~ git clone /media/vlc.git vlc
    Cloning into vlc...
    done.
    ~ cd vlc
    ~ ls
    AUTHORS    configure.ac  include      modules  src
    autotools  contrib       INSTALL      NEWS     test
    bin        COPYING       lib          po       THANKS
    bindings   COPYING.LIB   m4           projects
    bootstrap  doc           make-alias   README
    compat     extras        Makefile.am  share

We should now bootstrap, configure and build VLC media player the usual way:

    :::shell
    ~ ./bootstrap
    generating modules/**/Makefile.am
    .....................................................
    + echo
    + cp -f INSTALL INSTALL.git
    [...]
    + echo
    + mv -f INSTALL.git INSTALL
    + rm -f 'stamp-h*'
    + set +x
    Successfully bootstrapped

Then configure VLC media player in a sub-directory:

    :::shell
    ~ mkdir build
    ~ cd build
    ~ ../configure ..
    checking build system type... x86_64-unknown-linux-gnu
    checking host system type... x86_64-unknown-linux-gnu
    checking for a BSD-compatible install... /usr/bin/install -c
    checking whether build environment is sane... yes
    checking for a thread-safe mkdir -p... /bin/mkdir -p
    checking for gawk... gawk
    checking whether make sets $(MAKE)... yes
    checking how to create a ustar tar archive... gnutar
    checking whether to enable maintainer-specific portions of Makefiles... yes
    checking for style of include used by make... GNU
    checking for gcc... gcc
    checking whether the C compiler works... yes
    checking for C compiler default output file name... a.out
    [...]
    libvlc configuration
    --------------------
    version               : 2.1.0-git
    system                : linux
    architecture          : x86_64 mmx sse sse2
    optimizations         : yes
    vlc aliases           : cvlc rvlc qvlc svlc nvlc
    To build vlc and its plugins, type `make', or `./compile' if you like nice colors.

We disabled mkv and avcodec as libeml and libavcodec versions provided by
Debian (in this version of the distribution) are too old.

In order to correctly test VLC, you would want to compile ffmpeg yourself and
to give it to the configure script. This is already explained in the article
about [compilation for Linux
users]({filename}0007_compiling_vlc_for_linux_people.md).

Then compiling VLC media player:

    :::shell
    ~ make -j4
    [...]

Testing VLC
===========

Running VLC media player under PRoot does not change the way you will test it.
In fact most of the tests works the same way in the host root file system or in
the guest file system.

You can for example run the VLC automatic tests:

    :::shell
    ~ make check
    [...]

Future work
===========

In the two first article we looked at a way to setup a root file system in
order to compile and test a software in this environment.

In the following articles we will extend the possibilities of this method
thanks to Qemu. Using Qemu along with PRoot we will be able to test softwares
compiled for a different processor like ARM or SH4.
