Title: VLC media player extension for LibriVox
Date: 2010-09-13 19:17
Category: VLC media player
Tags: Extension, lua, VideoLAN, VLC
Slug: VLC-media-player-extension-for-LibriVox

Thanks to VLC extensions, we recently created a script that fetch books from
LibriVox. This website is a directory and a project that provide free audio
books from the Public Domain like [Frankenstein (Mary
Shelley)](http://librivox.org/frankenstein-or-modern-prometheus-by-mary-w-shelley/)
or [War and Peace (Leo
Tolstoy)](http://librivox.org/newcatalog/search.php?reader=&mc=&bc=&cat=&genre=&language=&type=&author=tolstoy&title=&status=complete&reader_exact=&mc_exact=&bc_exact=&date=&group=&engroup=&ingroup=&group=141).

LibriVox
========

[LibriVox](http://librivox.org) is a website in which you can find and download
free audio books. These audio books are all available in the public domain,
which means that most of the books were published before 1923. The audio books
are also donated to the public domain.

These audio books are read and recorded by volunteers from all over the world.
If you wish to participate, just go to
[http://librivox.org](http://librivox.org) and follow the instructions to
participates.

Catalog
-------

The full catalog of LibriVox is available on the website. Actually 3238 audio
books are available:

* Franz Kafka: [The
  Metamorphosis](http://librivox.org/the-metamorphosis-by-franz-kafka/)
(English and German)
* Victor Hugo: [Les
  misérables](http://librivox.org/les-miserables-vol-1-by-victor-hugo/)
(English and French)
* Immanuel Kant: [Critique of Practical
  Reason](http://librivox.org/critique-of-practical-reason-by-immanuel-kant/)

VLC extension
=============

As we really like the idea of free audio books from the public domain, we wrote
an extension that allows VLC users to hear LibriVox audio books directly inside
VLC media player.

This extension grab the podcast file from the LibriVox website and extract a
list of books. When opening the extension we can see:

![LibriVox]({filename}/rsc/librivox.png)

A directory is created for each book and contains the associated chapters. When
double-clicking on a book you will see the tracks for the corresponding
chapters:

![Selecting]({filename}/rsc/librivox_selection.png)

Getting the extension
=====================

For the moment, the extension is **not** distributed along with VLC. You can
download the extension from the Git repository
([librivox.lua](http://git.videolan.org/?p=vlc.git;a=blob_plain;f=share/lua/sd/librivox.lua;h=138ca4c42e6f7f009c2f2161b1c3a1610111346b;hb=HEAD))
and put the file in the *share/lua/sd* directory.
