Title: Multicat
Date: 2010-09-02 13:27
Category: VideoLAN
Tags: Debian, Multicat, Streaming, VideoLAN
Slug: Multicat

Multicat is an equivalent of the famous netcat but designed with streaming and
especially RTP in mind.

A short introduction
====================

Multicat is an application that enables you to take any stream you want and
re-stream it. This application takes one input and give one output.  The overall
diagram might be the following:

![Multicat]({filename}/rsc/multicat.png)

Use cases
=========

Even if Multicat is really simple, it can be very useful to manipulate streams
of any kind.

Restreaming
-----------

Multicat manipulate unicast and multicast streams without demuxing or
consideration on the transported data.

Transform a multicast stream coming from 239.255.0.1 to a unicast one on
192.168.0.1:

    :::shell
    $ multicat @239.255.0.1:5004 192.168.0.1:5005

Grab a unicast stream from 192.168.0.1 and multicast it to 239.255.0.22:

    :::shell
    $ multicat 192.168.0.1:4212 @239.255.0.22:2112

Recording a stream
------------------

Multicat can be used to record a stream. It only dump the stream on the disk
without any processing. However, Multicat create an auxiliary file that
contains the timestamp of each packet it receives and dumps. This file can be
later used to stream the file at the right speed.

![Recording]({filename}/rsc/multicat_save.png)

    :::shell
    $ multicat @239.255.0.1:5004 /record/stream.ts

Multicat will also create */record/stream.aux* file.

Streaming
---------

That's also possible to stream a file previously saved:

![Streaming]({filename}/rsc/multicat_stream.png)

    :::shell
    $ multicat -p 68 /record/stream.ts 192.168.0.1:4122
    # or for multicast
    $ multicat -p 68 /record/stream.ts @239.255.0.26:1234

Getting multicat
================

As part of the [VideoLAN project](http://www.videolan.org), Multicat can be
found on the [Multicat home
page](http://www.videolan.org/projects/multicat.html).

A Debian package has been created but it's still waiting in the NEW queue for
the moment.
