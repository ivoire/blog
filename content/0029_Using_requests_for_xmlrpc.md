Title: Using requests with xmlrpc
Date: 2017-07-25 10:33
Category: Python
Tags: Linaro, Python, LAVA, xmlrpc
Slug: Using-requests-with-xmlrpc

Using XML-RPC with Python3 is really simple. Calling **system.version** on
**http://localhost/RCP2** is as simple as:

    :::python3
    import xmlrpc.client

    proxy = xmlrpc.client.ServerProxy("http://localhost/RPC2")
    print(proxy.system.version())

However, the default client is missing many features, like handling proxies.
Using requests for the underlying connection allows for greater control of the
http request.

The xmlrpc client allows to change the underlying transport class by a custom
class. In order to use requests, we create a simple Transport class:

    :::python3
    import requests
    import xmlrpc.client

    class RequestsTransport(xmlrpc.client.Transport):

        def request(self, host, handler, data, verbose=False):
            # set the headers, including the user-agent
            headers = {"User-Agent": "my-user-agent",
                       "Content-Type": "text/xml",
                       "Accept-Encoding": "gzip"}
            url = "https://%s%s" % (host, handler)
            try:
                response = None
                response = requests.post(url, data=data, headers=headers)
                response.raise_for_status()
                return self.parse_response(response)
            except requests.RequestException as e:
                if response is None:
                    raise xmlrpc.client.ProtocolError(url, 500, str(e), "")
                else:
                    raise xmlrpc.client.ProtocolError(url, response.status_code,
                                                      str(e), response.headers)

        def parse_response(self, resp):
            """
            Parse the xmlrpc response.
            """
            p, u = self.getparser()
            p.feed(resp.text)
            p.close()
            return u.close()


To use this Transport class, we should use:

    :::python3
    proxy = xmlrpc.client.ServerProxy(uri, transport=RequestsTransport())

We can now use **requests** to:

* use proxies
* skip ssl verification (on a development server) or adding the right certificate chain
* set the headers
* set the timeouts
* ...

See the [documentation](https://requests.readthedocs.io/en/master/) or an
[example](https://framagit.org/ivoire/lavacli/blob/945341e1c12e39636b082b84d73942b69f03b156/lavacli/__init__.py#L44)
for more information.
