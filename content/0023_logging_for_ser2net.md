Title: Logging for ser2net
Date: 2016-06-22 16:12
Category: LAVA
Tags: LAVA, ser2net, Linaro
Slug: Logging-for-ser2net

Today, I was debugging an strange issue in
[LAVA](https://validation.linaro.org) while trying to boot a board.

I was under the impression that the board was not receiving all the u-boot
commands that LAVA was sending.

The serial connection is accessible on the network by
[ser2net](http://ser2net.sourceforge.net/).

I discovered that ser2net can log inputs, outputs or both to a file:

    TRACEFILE:trace1:/var/log/ser2net.2000.log
    2000:telnet:0:/dev/ttyUSB0:9600 tr=trace1

*ser2net* will print both inputs and outputs characters to
*/var/log/ser2net.2000.log*.


Going back to my issue, the logfile was:

    sseetteennvv  aauuttoollooaadd  nnoo
    sseettenv initrd_high '0xffffffff'

When the characters are doubled, it means that *ser2net* has sent **and**
received that character.
That's what I was expected as the board is supposed to *echo* all characters.

However, the characters are suddenly no longer doubled: the board is not
echoing. This does explain why the board was not booting.

The question remains: why is the serial line breaking after a random number of
characters.
