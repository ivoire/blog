Title: Visualization in VLC
Date: 2010-02-01 15:21
Category: VLC Media Player
Tags: VLC, VideoLAN, GSoC
Slug: Visualisations-in-VLC

Last summer, I worked on VLC media player as part of the Google Summer of
Code, One part of my job was to integrate libprojectM inside VLC media
player.

libprojectM is a library that create visualization according to the samples it
receive. As you can see the results are really amazing. And for the next
version of VLC media player you will normally see this nice feature. Lest
enjoy a first screenshot!

![ProjectM screenshot]({filename}/rsc/projectm_screenshot.jpg)
