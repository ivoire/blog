Title: Born to be PRoot
Date: 2012-04-09 17:01
Category: PRoot
Tags: PRoot, Linaro
Slug: Born-to-be-PRoot

It's been a long time since my last post on this blog. To start again in a good
shape I will seize the opportunity of a new version of [PRoot](http://proot.me)
to present this open source software in a cycle of articles.

What is PRoot about?
===================

PRoot is a **user-space** implementation of:

* chroot
* mount --bind
* binfmt_misc

This mean that PRoot allows you to:

* Change the root filesystem of a process.
* Bind some files to another location in the file system.
* Transparently execute binaries built for another CPU architecture
  through Qemu.

PRoot is based on the unprivileged system-call **PTrace**, making these three
features available for unprivileged users.

Use cases
=========

I found some really interesting uses cases of PRoot for a software developer
and Linux user that I will describe in future blog posts:

* [Testing another distribution that the one currently
  installed]({filename}0014_proot_sorcery.md)
* [Validating VLC on many Linux
  distributions]({filename}0015_making_vlc_at_home.md)
* Compiling VLC natively for ARM on x86
