Title: Compiling VLC for linux people
Date: 2010-10-01 13:13
Category: VLC media player
Tags: Compilation, VideoLAN, VLC
Slug: Compiling-VLC-for-linux-people

Compiling VLC media player on Linux is now something really easy. This article
is a small tutorial for every people wondering about it.

VLC versions
============

First, you have to choose which version of VLC you would like to compile as at
least two versions can be useful to compile

* *unstable* or *trunk*: development version. This version can be
  really broken or really stable depending on what we did the last
  days. Moreover a lot of new features are available in this version
  but not in the stable one. If you want to use VLC in a production
  environment this is not the version you are looking for unless you
  need a special feature. When this version is stable enough and add
  nice features to the stable version, it becomes the new stable
  version.
* *stable* (currently vlc 1.1.5-git): this is the version officially
  released. This version is stable and can be used in a production
  environment.
* older versions: we discourage the use of older version as bugs are
  no longer fixed for these versions.

Compiling VLC
=============

Some important tools and libraries
-------------------------------

To compile VLC some tools are required:

* automake
* autoconf
* libtool
* pkgconfig
* A C and C++ compiler (gcc for example)

On a Debian or Ubuntu system, just install *build-essential* package:

    :::shell
    $ apt-get install build-essential

Moreover VLC features depends on a long list of external libraries. Having all
these libraries is not mandatory to run VLC but some of them are really
important:

* ffmpeg (libavcodec, libavformat, libavutil): most of the codecs
* Qt4: Graphical User Interface
* xcb: video output under X11
* taglib: tags manipulation
* Lua: programming language for VLC extensions
* ...

Running this command will install most of VLC dependencies:

    :::shell
    $ apt-get build-dep vlc

Getting the source code
-----------------------

The easiest way to get VLC source code (and to keep it up to date) is to
use [Git](http://en.wikipedia.org/wiki/Git_%28software%29).

* VLC trunk:

    :::shell
    $ git clone git://git.videolan.org/vlc.git vlc-source

* VLC stable (currently 1.1.5-git)

    :::shell
    $ git clone git://git.videolan.org/vlc/vlc-1.1.git vlc-source

Bootstraping
------------

The *bootstrap* operation is the first thing to do after the download of the
VLC source code. This operation will check your system for required softwares
and create some missing files needed for the next steps.

    :::shell
    $ cd vlc-source
    $ ./bootstrap
      + dirname ./bootstrap
      + cd .
      + ACLOCAL_ARGS=-I m4
      + test -d extras/contrib/build/bin
      + uname -s
      + test .Linux = .Darwin
      + pkg-config --version
      + PKGCONFIG=yes
      + export AUTOPOINT
      + test
      + AUTOPOINT=autopoint
      + autopoint --dry-run --force
      + set +x
      generating modules/**/Makefile.am
      .................................
    [...]

Configure
---------

The *configure* step checks the presence of required libraries and header
files like ffmpeg, taglib and many other libraries. Depending on the presence
of a given library, this step will configure the compilation of VLC to be done
with or without some features.

For example if taglib can't be detected on your system, the configure script
will disable the taglib meta reader module as it cannot compile.

You can disable or enable some features and modules by passing the right
arguments to the configure script:

* *--enable-debug*: enable debug symbols. This argument is really
  useful if you want to debug VLC. For a production environment that's better
to avoid it.
* *--enable-\**: there are plenty of --enable arguments, look at the
 configure.ac file for the existing ones. You can either enable or disable each
of them by using *--enable-foo* or *--disable-bar*.

The most common parameter is *enable-debug*:

    :::shell
    $ ./configure --enable-debug
      checking build system type... x86_64-unknown-linux-gnu
      checking host system type... x86_64-unknown-linux-gnu
      checking for a BSD-compatible install... /usr/bin/install -c
      checking whether build environment is sane... yes
      checking for a thread-safe mkdir -p... /bin/mkdir -p
      checking for gawk... no
      checking for mawk... mawk
      checking whether make sets $(MAKE)... yes
      checking how to create a ustar tar archive... gnutar
      checking for style of include used by make... GNU
      checking for gcc... gcc
      checking whether the C compiler works... yes
      checking for C compiler default output file name... a.out
      checking for suffix of executables...
      checking whether we are cross compiling... no
      checking for suffix of object files... o
    [...]


If an error stops the configure script, the error message will give you more
information and advice to solve the problem:

    :::shell
    checking a52dec/a52.h presence... no
    checking for a52dec/a52.h... no
    configure: error: Could not find liba52 on your system: you may get it from http://liba52.sf.net/.
    Alternatively you can use --disable-a52 to disable the a52 plugin.

In this case, installing *liba52* or adding *--disable-a52* to the configure
arguments will fix the error. Rerun the configure script to take into account
the changes.

Compiling
---------

The last but no the least: **compiling VLC**

    :::shell
    $ ./compile
      /bin/bash ./config.status --file=vlc-config
      config.status: creating vlc-config
      chmod 0755 vlc-config
      touch vlc-config
      make  all-recursive
      make[1]: Entering directory `vlc-source'
      Making all in po
      make[2]: Entering directory `vlc-source/po'
      make[2]: Leaving directory `vlc-source/po'
      Making all in compat
      make[2]: Entering directory `vlc-source/compat'
      rm -f dummy.c
      echo '/* Automatically generated */' > dummy.c
      make  all-am
      make[3]: Entering directory 'vlc-source/compat'
      CC dummy.lo
      CC strlcpy.lo
      CCLD libcompat.la
    [...]

You can add *-jN* if you want to parallelize the compilation, where *N* is
usually the number of cores you have on your system plus one.

Compiling extra libraries
=========================

Instead of using the version from the distribution you might want to compile
one library used by VLC like ffmpeg. For example I'm using the trunk version of
ffmpeg instead of the one from my distribution.

First compile ffmpeg trunk version:

    :::shell
    $ svn checkout svn://svn.ffmpeg.org/ffmpeg/trunk ffmpeg
    $ cd ffmpeg
    $ ./configure --prefix=install_directory
    $ make install

The configure option *--prefix* specify the directory in which installing a
library. In this case choose a directory in which you can write, like a
sub-directory of you home. Then run the configure script of VLC with the right
variable:

    :::shell
    $ PKG_CONFIG_PATH=install_directory/lib/pkgconfig ./configure same_options_as_before

VLC will use the libraries from the *install_directory* and then the one from
the usual places. In this case, only ffmpeg is built from source: VLC will use
ffmpeg from the *install_directory* and the other libraries from the usual
places.

Coding for VLC
==============

You are now ready to fix every bug you encounter with VLC and to implement the
features you are dreaming about. Feel free to send us every modification you
made, they are always very welcome.
