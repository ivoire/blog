<?php
function redirect($url, $permanent = false)
{
  header('Location: ' . $url, $permanent ? 301 : 302);
  exit();
}

if(count($_GET) == 1)
  redirect('http://ivoire.dinauz.org/blog/' . key($_GET));
else
  redirect('http://ivoire.dinauz.org/blog/');
?>
