Title: @FOSDEM
Date: 2014-01-08 15:20
Category: PRoot
Tags: CARE, PRoot
Slug: @FOSDEM

We will be at FOSDEM to talk about [PRoot](http://proot.me) and some other
tools based on it.

Cedric will do a Lightning talk Saturday about [syscall
instrumentation](https://fosdem.org/2014/schedule/event/syscall/).  He will
present some tools, based on syscall instrumentation, that we develop and use
at STMicroelectronics:

* [PRoot](http://proot.me), emulate chroot, bind mount and
  binfmt_misc for non-root users
* [CARE](http://reproducible.io) (aka. Comprehensive Archiver for
  Reproducible Execution), creates automatically an archive that
  contains all the material required to re-execute the monitored
  programs in their original context (environment, files, expected
  kernel features, ...)
* DepsTracker (to be released soon), observes the execution of any
  processes in order to compute their mutual dependencies with respect
  to the file-system

Hope to see you there.
