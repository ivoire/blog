Title: VideoLAN accepted for Google Code In
Date: 2010-11-06 14:08
Category: VideoLAN
Tags: GCi, GSoC, VideoLAN, VLC
Slug: VideoLAN-accepted-for-Google-Code-In

Google just released the list of organizations that will participate to the
**Google Code In** contest and VideoLAN is part of it!

Google Code In
==============

I guess most people knows about the *Google Summer of Code* contest that Google
runs every summer. For the one who don't know, every summer, Google select some
students (above 18 years old) and pay them to work on Open Source Projects like
VLC media player, VLMC or libx264.

The goal of the *Google Code In* is really different as it target only people
from 13 to 17 year old. Moreover the goal is not only to produce lines of code
but covers a lot of subjects like:

* Code: writing or refactoring some part of the code base
* Documentation: a lot of work is need to create a good documentation
* Translation: helping the team of translators
* And many more ...

You can find some more information on the [Google Code
In](http://code.google.com/intl/fr/opensource/gci/2010-11/index.html)
announce.

VideoLAN
========

VideoLAN has been selected to participate to the *Google Code In*. Some ideas
for the tasks are already available on the [VideoLAN
wiki](http://wiki.videolan.org/GCodeIn_Ideas) with the corresponding mentor.
